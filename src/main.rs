use lambda_runtime::{handler_fn, Context, Error};
use reqwest::Error as ReqwestError;
use serde::{Deserialize, Serialize};
use serde_json::{Value, json};

#[derive(Serialize)]
struct RequestBody {
    inputs: String,
}

#[derive(Deserialize, Debug)]
struct ApiResponseItem {
    generated_text: String,
}

async fn function_handler(event: Value, _: Context) -> Result<Value, Error> {
    let client = reqwest::Client::new();
    let api_key = std::env::var("HF_API_KEY").map_err(|_| "HF_API_KEY not set")?;

    let request_body = RequestBody {
        inputs: event["text"].as_str().unwrap_or("Once upon a time,").to_string(),
    };

    let response = client
        .post("https://api-inference.huggingface.co/models/openai-community/gpt2")
        .header("Authorization", format!("Bearer {}", api_key))
        .json(&request_body)
        .send()
        .await
        .map_err(|e| e.to_string())?;

    let parsed_response: Vec<ApiResponseItem> = response
        .json()
        .await
        .map_err(|e| e.to_string())?;

    let generated_text = parsed_response
        .first()
        .map_or("No text generated".to_string(), |item| item.generated_text.clone());

    Ok(json!({ "generated_text": generated_text }))
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(function_handler);
    lambda_runtime::run(func).await?;
    Ok(())
}
