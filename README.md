#  Rust Serverless Transformer Endpoint
This Rust project is to call a model written with tansformers using api, then encapsulate it into docker, upload it to AWS ECR and use lambda function to link with the ECR, and finally use api to call this function.

- Hugging Face Model URL: https://huggingface.co/openai-community/gpt2?text=Once+upon+a+time%2C

## Part I. Dockerize Hugging Face Rust transformer

### 1. Basic Framework Building
Use `cargo new` to create a new folder. Then we need to choose the right model on Hugging Face and use our API KEY to call this model. Here I choose GPT-2 from OpenAI. It is a text generatiin model written in transformers library.

After picking the model, we need to add dependencies under `Cargo.toml`:
```
[dependencies]
reqwest = { version = "0.12.3", features = ["json"] }
tokio = { version = "1.37.0", features = ["full"] }
serde = { version = "1.0.197", features = ["derive"] }
serde_json = "1.0.115"
lambda_runtime = "0.11.1"
openssl = { version = "0.10.64", features = ["vendored"] }
```

### 2. `main.rs` Logic Building
Then we need to build the basic logic for this function.
```
async fn function_handler(event: Value, _: Context) -> Result<Value, Error> {
    let client = reqwest::Client::new();
    let api_key = std::env::var("HF_API_KEY").map_err(|_| "HF_API_KEY not set")?;

    let request_body = RequestBody {
        inputs: event["text"].as_str().unwrap_or("Once upon a time,").to_string(),
    };

    let response = client
        .post("https://api-inference.huggingface.co/models/openai-community/gpt2")
        .header("Authorization", format!("Bearer {}", api_key))
        .json(&request_body)
        .send()
        .await
        .map_err(|e| e.to_string())?;

    let parsed_response: Vec<ApiResponseItem> = response
        .json()
        .await
        .map_err(|e| e.to_string())?;

    let generated_text = parsed_response
        .first()
        .map_or("No text generated".to_string(), |item| item.generated_text.clone());

    Ok(json!({ "generated_text": generated_text }))
}
```
Here we define the input is a JSON format {"text":"Once upon a time,"}. And use API to trigger the model for response.

## Part II. Deploy container to AWS Lambda

### 3. Dockerfile 
Create a Dockerfile:
```
# Using Amazon Linux 2 as the build environment
FROM amazonlinux:2 as builder

# Install Rust, Perl and their necessary modules and compilation tools
RUN yum update -y && \
    yum install -y gcc gcc-c++ make perl perl-IPC-Cmd openssl-devel && \
    curl https://sh.rustup.rs -sSf | sh -s -- -y

# Set Rust environment variables
ENV PATH="/root/.cargo/bin:${PATH}"

WORKDIR /usr/src/myapp
COPY . .

# Build Rust application. The vendored feature of openssl-sys will compile OpenSSL from source and statically link it to the application.
RUN cargo build --release --bin week10proj

# Use AWS Lambda’s base image as the final image
FROM public.ecr.aws/lambda/provided:al2

COPY --from=builder /usr/src/myapp/target/release/week10proj /var/runtime/bootstrap

# Set execution permissions
RUN chmod 755 /var/runtime/bootstrap

# ENV HF_API_KEYE=

CMD [ "week10proj.function_handler" ]
```

- Here we need to use Amazon Linux 2 to simulate the actual lambda environment.

### 4. Build Docker Image
Run `docker build -t week10proj .` in the terminal to build the image. And we may see the following output:
```
[+] Building 118.9s (15/15) FINISHED                       docker:desktop-linux
 => [internal] load build definition from Dockerfile                       0.0s
 => => transferring dockerfile: 1.69kB                                     0.0s
 => [internal] load metadata for docker.io/library/amazonlinux:2           0.6s
 => [internal] load metadata for public.ecr.aws/lambda/provided:al2        1.1s
 => [auth] aws:: lambda/provided:pull token for public.ecr.aws             0.0s
 => [internal] load .dockerignore                                          0.0s
 => => transferring context: 2B                                            0.0s
 => [builder 1/5] FROM docker.io/library/amazonlinux:2@sha256:85825c659f9  0.0s
 => [internal] load build context                                          0.2s
 => => transferring context: 270.82kB                                      0.2s
 => [stage-1 1/3] FROM public.ecr.aws/lambda/provided:al2@sha256:042e76ab  0.0s
 => => resolve public.ecr.aws/lambda/provided:al2@sha256:042e76ab8f52824b  0.0s
 => CACHED [builder 2/5] RUN yum update -y &&     yum install -y gcc gcc-  0.0s
 => CACHED [builder 3/5] WORKDIR /usr/src/myapp                            0.0s
 => [builder 4/5] COPY . .                                                 7.8s
 => [builder 5/5] RUN cargo build --release --bin week10proj             109.0s
 => CACHED [stage-1 2/3] COPY --from=builder /usr/src/myapp/target/releas  0.0s 
 => [stage-1 3/3] RUN chmod 755 /var/runtime/bootstrap                     0.4s 
 => exporting to image                                                     0.1s 
 => => exporting layers                                                    0.1s 
 => => writing image sha256:2c7147e0091e251abc0a12b61c28b2312fafe6adcae9b  0.0s 
 => => naming to docker.io/library/week10proj                              0.0s 

View build details: docker-desktop://dashboard/build/desktop-linux/desktop-linux/4tpgs179acon4hw5b7mo8ykex

What's Next?
  View a summary of image vulnerabilities and recommendations → docker scout quickview
```

![screenshot of docker image](https://gitlab.com/estrellas939/week10proj/-/raw/main/pic/image.png?inline=false)

### 5. AWS Elastic Container Registry (ECR)
Next we need to push this image to AWS ECR. Create a private repo and follow the instructions:

- `aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 568937614068.dkr.ecr.us-east-1.amazonaws.com`
Output:
```Login Succeeded```

- `docker tag week10proj:latest 568937614068.dkr.ecr.us-east-1.amazonaws.com/week10proj-gpt2-5:latest`

- `docker push 568937614068.dkr.ecr.us-east-1.amazonaws.com/week10proj-gpt2-5:latest`
Output: 
```
The push refers to repository [568937614068.dkr.ecr.us-east-1.amazonaws.com/week10proj-gpt2-5]
0d7ac4ebb18f: Pushed 
59e530c8b37a: Pushed 
5ce4da6a49d4: Pushed 
804941a67e91: Pushed 
25a97811023e: Pushed 
latest: digest: sha256:07ba51e605042a6275f7d8dc89d409b8ee5e0b338fb4ed6c79eb7f9f0d823d32 size: 1579
```
Now the image has been pushed to AWS ECR successfully!

![screenshot of ecr](https://gitlab.com/estrellas939/week10proj/-/raw/main/pic/ecr.png?inline=false)

### 6. AWS Lambda and HTTP API
Then, we need to create an AWS Lambda function and link that image to this function. Then, add a HTTP API as the trigger.

![screenshot of lambda w. api](https://gitlab.com/estrellas939/week10proj/-/raw/main/pic/lambda.png?inline=false)

## Part III. Implement query endpoint

### 7. AWS Lambda Console Test
Now we can run a simple test from the AWS Lambda console.
The test file should look like this:
```
{
    "text":"Once upon a time,"
}
```

![screenshot of test](https://gitlab.com/estrellas939/week10proj/-/raw/main/pic/log.png?inline=false)

We can see that this lambda function can generate a response but not through API way.

### 8. CURL Test
Now we can use `cURL` in the terminal to trigger the lambda function via API.
- Run the following command to trigger API:
```
curl -X POST https://njq3wt4iqf.execute-api.us-east-1.amazonaws.com/default/week10proj-3 -H "Content-Type: application/json" -d '{"text": "Once upon a time,"}'
```
Output:
![screenshot of curl test](https://gitlab.com/estrellas939/week10proj/-/raw/main/pic/curl.png?inline=false)

Since GPT-2 is a random text generation model, we can get different response each time.