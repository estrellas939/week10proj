# Using Amazon Linux 2 as the build environment
FROM amazonlinux:2 as builder

# Install Rust, Perl and their necessary modules and compilation tools
RUN yum update -y && \
    yum install -y gcc gcc-c++ make perl perl-IPC-Cmd openssl-devel && \
    curl https://sh.rustup.rs -sSf | sh -s -- -y

# Set Rust environment variables
ENV PATH="/root/.cargo/bin:${PATH}"

WORKDIR /usr/src/myapp
COPY . .

# Build Rust application. The vendored feature of openssl-sys will compile OpenSSL from source and statically link it to the application.
RUN cargo build --release --bin week10proj

# Use AWS Lambda’s base image as the final image
FROM public.ecr.aws/lambda/provided:al2

COPY --from=builder /usr/src/myapp/target/release/week10proj /var/runtime/bootstrap

# Set execution permissions
RUN chmod 755 /var/runtime/bootstrap

# ENV HF_API_KEYE=

CMD [ "week10proj.function_handler" ]